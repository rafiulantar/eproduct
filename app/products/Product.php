<?php

namespace ANTAR\Product;

use PDO;
use PDOException;


class Product
{
    
    public $title;
    public $price;
    public $description;
    public $image;
    public $color;
    public $size;
    public $is_available;
    private $conn;

    public function __construct()
    {
        try {
            session_start();
            $this->conn = new PDO("mysql:host=localhost;dbname=shop", "root", "");
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //            if($this->conn){
            //                echo 'Database connected';
            //            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function index()
    {

        $query = "SELECT * FROM product";
        $stmt = $this->conn->query($query);
        $product = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $product;
    }

    public function setData(array $data = [])
    {
        $error = [];
        if (array_key_exists('title', $data)) {
            if (strlen($data['title']) < 4) {
                $error[] = 'Title must be at least 4 characters';
            } else {
                $this->title = $data['title'];
            }
        } else {
            $error[] = 'Title is required.';
        }
        $this->price = $data['price'];
        $this->description = $data['description'];
        $this->image = $data['image'];
        $this->color = $data['color'];
        $this->size = $data['size'];
        $this->is_available = $data['is_available'];


        //        if(array_key_exists('email', $data)){
        //            if(filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
        //                $error[] = 'Invalid email address';
        //            }
        //        }else{
        //            $error[] = 'Email is required.';
        //        }

        if (count($error) > 0) {
            $_SESSION['errors'][] = $error;
            return false;
        }

        return $this;
    }

    public function store()
    {

        try {

            $query = "INSERT INTO product(title,price,description,image,color,size,is_available) 
                        VALUES(:title,:price,:description,:image,:color,:size,:is_available)";

            $stmt = $this->conn->prepare($query);

            $stmt->execute(array(
                ':title' => $this->title,
                ':price' => $this->price,
                ':description' => $this->description,
                ':image' => $this->image,
                ':color' => $this->color,
                ':size' => $this->size,
                ':is_available' => $this->is_available,
            ));

            $_SESSION['status']='Inserted Successfully';
            header('Location : index.php');
            
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }

    public function show($id)
    {
        $query = "SELECT * FROM product where id=" . $id;
        $stmt = $this->conn->query($query);
        $product = $stmt->fetch(PDO::FETCH_ASSOC);
        return $product;
    }

    public function update($id)
    {

        try {
            $query = "UPDATE users SET name=:username where id=" . $id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':username' => $this->userName,
            ));
            $_SESSION['status'] = 'Updated Successfully';
            header('Location: index.php');
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }

    public function delete($id)
    {
        try {
            $query = "Delete from product where id=" . $id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $_SESSION['status'] = 'Deleted Successfully';
            header('Location: index.php');
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }
}
