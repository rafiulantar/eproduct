<?php


include '../../vendor/autoload.php';
use ANTAR\Product\Product;

$Product = new Product();

$requestData = $Product->setData($_POST);

if ($requestData) {
    $requestData->store();
} else {
    header('Location: create.php');
}



?>
