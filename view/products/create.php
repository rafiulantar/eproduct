<?php
session_start();
////print_r($_SESSION);
if (isset($_SESSION['errors'])) {
    foreach ($_SESSION['errors'] as $error) {
        echo $error . '<br>';
    }
    unset($_SESSION['errors']);
}
?>


<a href="index.php">User List</a><br>
<form action="store.php" method="post">
    <fieldset>
        <legend>Add New Product</legend>
        <input type="text" name="title" placeholder="Enter Product title">
        <br><br>
        <input type="number" name="price" placeholder="Enter Product price">
        <br><br>
        <textarea name="description" placeholder="Product description" rows="4" cols="50">
        </textarea>
        <br><br>
        <input type="file" name="image" placeholder="Enter Product image">
        <br><br>
        <label for="color">Color:</label>
        <select name="color">
            <option value="Red">Red</option>
            <option value="Yellow">Yellow</option>
            <option value="Blue">Blue</option>
            <option value="Black">Black</option>
            <option value="White">White</option>
        </select>
        <br><br>
        <label for="size">Size:</label>
        <select name="size">
            <option value="S">S</option>
            <option value="M">M</option>
            <option value="L">L</option>
            <option value="XL">XL</option>
            <option value="XXL">XXL</option>
        </select>
        <br><br>
        <input type="radio" id="0" name="is_available" value="0">
        <label for="0">Unavailable</label>
        <input type="radio" id="1" name="is_available" value="1">
        <label for="1">Available</label><br>
        <br><br>
        <button type="submit">Save</button>
    </fieldset>
</form>