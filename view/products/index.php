<?php

include '../../vendor/autoload.php';

use ANTAR\Product\Product;

$product = new Product();
$products = $product->index();

?>
List of product <a href="create.php">Add New</a>
<hr>

<?php
if (isset($_SESSION['status'])) {
    echo $_SESSION['status'];
    unset($_SESSION['status']);
}
?>

<table border="1" cellpadding="5">
    <thead>
        <tr>
            <th>SL</th>
            <th>Name</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        //        if(isset($_SESSION['users'])){
        $sl = 0;
        foreach ($products as $key => $product) { ?>
            <tr>
                <td><?= ++$sl ?></td>
                <td><?php echo $product['title'] ?></td>
                <td>
                    <a href="show.php?id=<?= $product['id'] ?>">Show</a> |
                    <a href="edit.php?id=<?= $product['id'] ?>">Edit</a> |
                    <a href="delete.php?id=<?= $product['id'] ?>">Delete</a>
                </td>
            </tr 
            <?php
                };
                //        }
            ?>
        </tbody>
         
    </table>