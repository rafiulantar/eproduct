<?php
include '../../vendor/autoload.php';

use ANTAR\Product\Product;

$product = new Product();
$singleData = $user->show($_GET['id']);

////print_r($_SESSION);
if (isset($_SESSION['errors'])) {
    foreach ($_SESSION['errors'] as $error) {
        echo $error . '<br>';
    }
    unset($_SESSION['errors']);
}
?>



<a href="index.php">User List</a><br>

<form action="update.php?id=<?= $_GET['id'] ?>" method="post">
    <fieldset>

        <legend>Add New Product</legend>
        <input type="text" name="title" value="<?= $singleData['title'] ?>" placeholder="Enter Product title">
        <br><br>
        <input type="number" name="price" value="<?= $singleData['price'] ?>" placeholder="Enter Product price">
        <br><br>
        <textarea name="description " placeholder="Product description" rows="4" cols="50">
        <?= $singleData['description'] ?> </textarea>
        <br><br>
        <input type="file" name="image" value="<?= $singleData['image'] ?>" placeholder="Enter Product image">
        <br><br>
        <label for="color">Color:</label>
        <select name="color" id="color">
            <option value="Red" <?php if ($singleData['color'] == 'Red') {
                                    echo 'selected';
                                } ?>>Red</option>
            <option value="Yellow" <?php if ($singleData['color'] == 'Yellow') {
                                        echo 'selected';
                                    } ?>>Yellow</option>
            <option value="Blue" <?php if ($singleData['color'] == 'Blue') {
                                        echo 'selected';
                                    } ?>>Blue</option>
            <option value="Black" <?php if ($singleData['color'] == 'Black') {
                                        echo 'selected';
                                    } ?>>Black</option>
            <option value="White" <?php if ($singleData['color'] == 'White') {
                                        echo 'selected';
                                    } ?>>White</option>
        </select>
        <br><br>

        <label for="size">Size:</label>
        <select name="size" id="size">
            <option value="S">S</option>
            <option value="M">M</option>
            <option value="L">L</option>
            <option value="XL">XL</option>
            <option value="XXL">XXL</option>
        </select>
        <br><br>
        <input type="radio" id="0" name="is_available" <?php if ($singleData['is_available'] == 0) {
                                                            echo 'checked';
                                                        } ?> value="0">
        <label for="0">Unavailable</label>
        <input type="radio" id="1" name="is_available" <?php if ($singleData['is_available'] == 1) {
                                                            echo 'checked';
                                                        } ?> value="1">
        <label for="1">Available</label><br>
        <br><br>
        <button type="submit">Update</button>
    </fieldset>
</form>