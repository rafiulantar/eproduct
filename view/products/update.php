<?php
include '../../vendor/autoload.php';

use ANTAR\Product\Product;

$Product = new Product();
$requestData = $Product->setData($_POST);
if ($requestData) {
    //    die($_GET['id']);
    $Product->setData($_POST)->update($_GET['id']);
} else {
    header('Location: edit.php?id=' . $_GET['id']);
}
